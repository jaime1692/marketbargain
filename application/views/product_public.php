<?php
if(isset($item)){
    $product = $item->result()[0];
    $update = ($this->session->userdata('userid') == $product->user_id)? ' ' : ' disabled';
    $stars = "";
    for($i = 0; $i < 5;$i++){
        if( floor( $product->rating )-$i >= 1 ){ 
            $stars .= '<i class="fa fa-star"></i>'; 
        } else if( $product->rating-$i > 0 ){ 
            $stars .= '<i class="fas fa-star-half-alt"></i>'; 
        } else{
            $stars .= '<i class="fa fa-star-o"></i>'; 
        }
    }
}
?>
<div class="container">
    <h1>Product item</h1>
    <h4 style="color:#FF5F6D"> <?= ($product->rating)? $stars : "No rating available"; ?></h4>
    <div class="row">
        <div class="col">
            <!-- Product image -->
            <label for="productImage">Product image</label>
            <?php 
                $img_url = base_url().'assets/images/';
                if(isset($product->image)){
                    echo '<img src="'.$img_url.$product->image.'" class="card-img-top" alt="...">';    
                } else {
                    echo '<img src="'.$img_url.'no-photo.gif" class="card-img-top" alt="...">';
                }
            ?>
        </div>
        <div class="col">
            <div class="was-validated">
                <?php
                    if($update == ' '){
                        echo form_open_multipart('product/update_product','',$hidden = array('product' => $product->product_id));
                    } else {
                        echo form_open_multipart('main/add_item_cart','',$hidden =  array('productItem' => $product->product_id));
                    }
                ?>
                <!-- Product name -->
                <div class="form-group">
                    <label for="productName">Name</label>
                    <input type="text" class="form-control" name="productName" id="productName" value="<?= $product->name ?>" <?= $update; ?>>
                </div>
                <!-- Description -->
                <div class="form-group mb-3">
                <label for="productDescription">Description</label>
                <textarea class="form-control" name="productDescription" id="productDescription" <?= $update; ?>><?= $product->description ?></textarea>
                </div>
                <div class="row">
                    <!-- Product type -->
                    <div class="col">
                        <label for="productType">Type</label>
                        <div class="container">
                            <div class="row">
                            <?= $product->type_name; ?>
                            </div>
                        </div>
                    </div>
                    <!-- Product category -->
                    <div class="col form-group">
                        <label for="productCategory">Category</label>
                        <div class="container">
                        <div class="row">
                            <?= $product->cat_name ?>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- Product quantity -->
                    <div class="col form-group">
                        <label for="productQty">Quantity</label>
                        <input type="number" class="form-control" name="productQty" id="productQty" value="<?= $product->quantity ?>" <?= $update; ?>>
                    </div>
                    <!-- Product price -->
                    <div class="col form-group">
                        <label for="productPrice">Price</label>
                        <input type="number" class="form-control" name="productPrice" id="productPrice" value="<?= $product->price ?>" <?= $update; ?>>
                    </div>
                </div>
                <?php
                 if($update == ' '){
                    echo '<button type="submit" class="btn btn-primary"style="float: right !important;margin-bottom: 50px;">Update product</button>';
                 } else {
                    echo '<button type="submit" class="btn btn-primary"style="float: right !important;margin-bottom: 50px;">Add product</button>';
                 }
                ?>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>