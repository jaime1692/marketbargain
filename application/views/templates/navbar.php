<nav class="navbar navbar-expand-lg navbar-light bg-light" style="z-index:2;">
  <a class="navbar-brand" href="<?php echo site_url('main') ?>">Market<strong>Bargain</strong></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('main') ?>">Home</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php
              foreach($categories->result() as $category){
                echo '<a class="dropdown-item" href="'.site_url('main/category/'.$category->category_id).'">'.$category->cat_name.'</a>';
              }
          ?>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Types
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php
              foreach($types->result() as $type){
                echo '<a class="dropdown-item" href="'.site_url('main/type/'.$type->type_id).'">'.$type->type_name.'</a>';
              }
          ?>
        </div>
      </li>
      <?php if(isset($user['username'])){ ?>
      <li class="nav-item">
        <?php echo form_open('main/search_item', 'class="form-inline my-2 my-lg-0"'); ?>
          <input class="typeahead form-control mr-sm-2" id="text" id="text" name="text" type="search" placeholder="Search" aria-label="Search" value="<?php echo (isset($search))? $search: ""; ?>">
          <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
        <?php echo form_close(); ?>   
      </li>
      <?php } ?>
    </ul>
    <?php if(isset($user['username'])){ ?>
    <form class="form-inline">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('cart') ?>"><i class="fas fa-shopping-cart"></i> <span class="badge badge-warning"><?php echo $cart; ?></span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="far fa-user-circle"></i> Welcome! <?php echo $user['username']; ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo site_url('profile') ?>">Profile</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo site_url('auth/logout') ?>">Logout</a>
        </div>
      </li>
    </ul>
    </form>
    <?php } ?>
  </div>
</nav>
<script type="text/javascript">
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.post("<?= site_url('main/autocomplete_box') ?>", { query: query }, function (data) {
                console.log(data);
                data = $.parseJSON(data);
                return process(data);
            });
        }
    });
</script>
<style>
.typeahead{
  width: 206px;
}
</style>