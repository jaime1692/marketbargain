    <?php
        if(isset($error)){
            echo '<ul>';
            echo "<li>$error</li>";
            echo '</ul>';
        }
    ?>

<div class="container">
    <h1>New product</h1>
    <div class="row">
        <div class="col">
            <!-- Product image -->
            <?php echo form_open_multipart('product/do_upload'); ?>
                <label for="productImage">Product image</label>
                <?php 
                    $img_url = base_url().'assets/images/';
                    if(isset($upload_data)){
                        echo '<img src="'.$img_url.$upload_data['file_name'].'" class="card-img-top" alt="...">';    
                    } else {
                        echo '<img src="'.$img_url.'no-photo.gif" class="card-img-top" alt="...">';
                    }
                ?>
                <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="validatedCustomFile" required>
                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                <div class="invalid-feedback">Example invalid custom file feedback</div>
                </div>
                <button type="submit" value="upload" class="btn btn-outline-info">Upload photo</button>
            <?php echo form_close(); ?>
        </div>
        <div class="col">
            <div class="was-validated">
                <?php echo form_open_multipart('product/add_product'); ?>
                <!-- Product name -->
                <div class="form-group">
                    <label for="productName">Name</label>
                    <input type="text" class="form-control" name="productName" id="productName" placeholder="Product name" required>
                </div>
                <!-- Description -->
                <div class="form-group mb-3">
                <label for="productDescription">Description</label>
                <textarea class="form-control is-invalid" name="productDescription" id="productDescription" placeholder="Introduce a description of the product" required></textarea>
                <div class="invalid-feedback">
                    Please enter a message in the textarea.
                </div>
                </div>
                <div class="row">
                    <!-- Product type -->
                    <div class="col">
                        <label for="productType">Type</label>
                        <select class="custom-select" name="productType" id="productType" required>
                        <?php 
                            foreach($types->result() as $type){
                                echo '<option value="'.$type->type_id.'">';
                                echo $type->type_name;
                                echo '</option>';
                            }
                        ?>
                        </select>
                    </div>
                    <!-- Product category -->
                    <div class="col form-group">
                        <label for="productCategory">Category</label>
                        <select class="custom-select" name="productCategory" id="productCategory" required>
                            <?php 
                                foreach($categories->result() as $category){
                                    echo '<option value="'.$category->category_id.'">';
                                    echo $category->cat_name;
                                    echo '</option>';
                                }
                            ?>
                        </select>
                        <div class="invalid-feedback">Example invalid custom select feedback</div>
                    </div>
                </div>
                <div class="row">
                    <!-- Product quantity -->
                    <div class="col form-group">
                        <label for="productQty">Quantity</label>
                        <input type="number" class="form-control" name="productQty" id="productQty" required>
                    </div>
                    <!-- Product price -->
                    <div class="col form-group">
                        <label for="productPrice">Price</label>
                        <input type="number" class="form-control" name="productPrice" id="productPrice" required>
                    </div>
                </div>
                
                <!-- Producto image name -->
                <div class="form-group">
                <?php 
                    $img_url = base_url().'assets/images/';
                    if(isset($upload_data)){
                        echo '<input class="form-control" style="display:none;" type="checkbox" id="productImage" name="productImage" value="'.$upload_data['file_name'].'" checked required>';
                        echo '<div class="valid-feedback"><h1><i class="far fa-image"></i></h1> Photo successfully added.</div>';
                    } else {
                        echo '<input class="form-control" style="display:none;" type="checkbox" id="productImage" name="productImage" value="no-photo.gif" required>';
                    }
                ?>
                <div class="invalid-feedback"><h1><i class="far fa-image"></i></h1> Please add an image for the product.</div>
                </div>
                <button type="submit" class="btn btn-primary"style="float: right !important;margin-bottom: 50px;">Add product</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>