<?php $cart_array = $cart->result_array(); $cart = $cart->result();?>
<div class="container">
<h1>Saved wishlist</h1>
<h2><?= $wishlist->result()[0]->name ?> <!--<a href="#"><i class="far fa-edit"></i></a>--></h2>
<p><?= $wishlist->result()[0]->date ?></p>
<table class="table table-borderless">
  <thead class="thead-dark">
    <tr>
      <th scope="col"></th>
      <th scope="col">Name</th>
      <th scope="col">Description</th>
      <th scope="col">Type</th>
      <th scope="col">Available</th>
      <th scope="col">Qty</th>
      <th scope="col">Price</th>
    </tr>
  </thead>
  <tbody>
    <?php
        $total = 0;
        if(isset($cart)){
            for($i=0;$i < count($cart); $i++){
              $total += $cart[$i]->price * $cart[$i]->wish_quantity;
                echo '<tr>';
                echo '<td><img src="'.base_url().'/assets/images/'.$cart[$i]->image.'" alt="Smiley face" height="42" width="42"></td>';
                echo '<td>'.$cart[$i]->name.'</td>';
                echo '<td>'.substr($cart[$i]->description, 0, 50).' <a href="'.site_url('product/publication/'.$cart[$i]->product_id).'">See more<a></td>';
                echo '<td>'.$cart[$i]->type_name.'</td>';
                if ($cart[$i]->wish_quantity > $cart[$i]->quantity){                
                  echo '<td style="color:red;font-weight:bold;">'.$cart[$i]->quantity.'<i class="fas fa-exclamation-circle"></i></td>';
                } else {
                  echo '<td>'.$cart[$i]->quantity.'</td>';
                }
                echo '<td>'.$cart[$i]->wish_quantity.'</td>';
                echo '<td>'. $cart[$i]->price.'</td>';
                echo '</tr>';
            }
          }
          ?>
  </tbody>
</table>
<?php echo '<h3 align="right"> Total price $'.$total.'</h3>'; ?>
<div class="row" style="margin-left: 71%;">
    <!--<?= form_open('cart/delete_wishlist') ?><button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i> Delete wishlist</button><?= form_close() ?>-->
    <?= form_open_multipart('checkout') ?><input name="cart" type="text" value="<?= base64_encode(serialize($cart_array)); ?>" hidden><button type="submit" class="btn btn-info">Next step <i class="fas fa-arrow-right"></i></button><?= form_close() ?>
</div>
</div>