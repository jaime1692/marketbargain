<div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <?php
      $i = 0;
      foreach($offers as $product){
        if($i == 0){
          echo '<div class="carousel-item active" data-interval="10000">';
        } else {
          echo '<div class="carousel-item" data-interval="10000">';
        }
        echo '<img src="'.base_url('assets/images/'.$product->image).'" class="d-block w-100" alt="...">';
        echo '<div class="carousel-caption d-none d-md-block">';
        echo '<h5>'.$product->name.'</h5>';
        echo '<p>'.$product->description.'</p>';
        echo '<a href="'.site_url('product/publication/'.$product->product_id).'" class="btn btn-outline-warning" >Explore</a>';
        echo '</div>';
        echo '<div class="promotion"><h5>'.$product->promotion.'%</h5></div>';
        echo '</div>';
        $i++;
      }

    ?>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<script>
$('.carousel').carousel()
</script>