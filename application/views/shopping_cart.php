<div class="container">
<h1>Shopping cart</h1>
<table class="table table-borderless">
  <thead class="thead-dark">
    <tr>
      <th scope="col"></th>
      <th scope="col">Name</th>
      <th scope="col">Description</th>
      <th scope="col">Type</th>
      <th scope="col">Available</th>
      <th scope="col">Qty</th>
      <th scope="col">Price</th>
    </tr>
  </thead>
  <tbody>
    <?php
        $total = 0;
        $cart_array = array();
        if(isset($cart[1])){
          for($i=1;$i < count($cart); $i++){
              $cart_array[] = $cart[$i];
              $total += $cart[$i]['price'] * $cart[$i]['wish_quantity'];
                echo '<tr>';
                echo '<td><img src="'.base_url().'/assets/images/'.$cart[$i]['image'].'" alt="Smiley face" height="42" width="42"></td>';
                echo '<td>'.$cart[$i]['name'].'</td>';
                echo '<td>'.substr($cart[$i]['description'], 0, 50).' <a href="'.site_url('product/publication/'.$cart[$i]['product_id']).'">See more<a></td>';
                echo '<td>'.$cart[$i]['type_name'].'</td>';
                if ($cart[$i]['wish_quantity'] > $cart[$i]['offert']){                
                  echo '<td style="color:red;font-weight:bold;">'.$cart[$i]['offert'].'<i class="fas fa-exclamation-circle"></i></td>';
                } else {
                  echo '<td>'.$cart[$i]['offert'].'</td>';
                }
                echo '<td class="cart-update">'.form_open('cart/minus_one').form_hidden('id',  $cart[$i]['product_id']).'<button type="submit"><i class="fas fa-minus"></i></button>'.form_close().'<p>'.$cart[$i]['wish_quantity'].'</p>'.form_open('cart/plus_one').form_hidden('id',  $cart[$i]['product_id']).'<button type="submit"> <i class="fas fa-plus"></i></button>'.form_close().'</td>';
                echo '<td>'. $cart[$i]['price'].'</td>';
                echo '</tr>';
            }
          }
          ?>
  </tbody>
</table>
<?php echo '<h3 align="right"> Total price $'.$total.'</h3>'; ?>
<div class="row" style="margin-left: 71%;">
    <?= form_open('cart/save_wishlist') ?><button type="submit" class="btn btn-secondary"><i class="far fa-heart"></i> Save to wishlist</button><?= form_close() ?>
    <?= form_open_multipart('checkout') ?><input name="cart" type="text" value="<?= base64_encode(serialize($cart_array)); ?>" hidden><button type="submit" class="btn btn-info">Next step <i class="fas fa-arrow-right"></i></button><?= form_close() ?>
</div>
</div>