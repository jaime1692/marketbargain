<div class="container">
<h1>Shipping details</h1>
<?= form_open('checkout/do_insert_address'); ?>
  <div class="form-row">
    <div class="col-md-4">
      <label for="userFirst">First name</label>
      <input type="text" class="form-control" name="userFirst" id="userFirst" required>
    </div>
    <div class="col-md-4">
      <label for="userLast">Last name</label>
      <input type="text" class="form-control" name="userLast" id="userLast" required>
    </div>
    <div class="col-md-4">
      <label for="userLast">Phone number</label>
      <input type="number" class="form-control" name="userPhone" id="userPhone" required>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-4">
      <label for="userUnit">Unit</label>
      <input type="number" class="form-control" name="userUnit" id="userUnit" required>
    </div>
    <div class="col-md-4">
      <label for="userNumber">Number</label>
      <input type="text" class="form-control" name="userNumber" id="userNumber"  required>
    </div>
    <div class="col-md-4">
      <label for="userStreet">Street</label>
      <input type="text" class="form-control" name="userStreet" id="userStreet" required>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="userCity">City/Suburb</label>
      <input type="text" class="form-control" name="userCity" id="userCity" required>
    </div>
    <div class="col-md-3 mb-3">
      <label for="userState">Country</label>
      <input type="text" class="form-control" name="userCountry" id="userCountry" required>
    </div>
    <div class="col-md-3 mb-3">
      <label for="userPostal">Postal code</label>
      <input type="text" class="form-control" name="userPostal" id="userPostal" required>
    </div>
  </div>
  <input name="cart" type="text" value="<?= base64_encode(serialize($cart)); ?>" hidden>
  <button class="btn btn-primary" type="submit">Next step <i class="fas fa-arrow-right"></i></button>
<?= form_close(); ?>
</div>