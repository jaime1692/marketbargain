<div class="container">
    <h1>Orders</h1>
    <div class="list-group">
    <?php
        foreach($orders->result() as $order){
            echo '<a href="'.site_url('profile/showOrder/'.$order->order_id).'" class="list-group-item d-flex list-group-item-action justify-content-between align-items-center">';
            echo '<h5 class="mb-1">'.$order->order_id.'</h5>';
            echo '<small>'.$order->datetime.'</small>';
            echo '<span class="badge badge-primary badge-pill">'.$order->total.'</span>';
            echo '</a>';
        }
        ?>
    </div>
</div>