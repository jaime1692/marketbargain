<div class="login container">
    <div class="login-form justify-content-center">
        <?php echo form_open('auth/login'); ?>
        <div class="form-group">
            <label for="username">Username or Email</label>
            <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" id="password" name="password" class="form-control" aria-describedby="passwordHelpInline">
            <small id="passwordHelpInline" class="text-muted">
            Must be 8-20 characters long.
            </small>
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="rememberme" name="rememberme">
            <label class="form-check-label" for="rememberme">Remember me</label>
        </div>
        <?php echo '<a class="btn btn-outline-secondary" href="'.site_url('auth/register').'">Register</a>';?>
        <button type="submit" class="btn btn-warning">Login</button>
        <?php echo form_close(); ?>
    <div>
</div>
<style>
body{
    background-size: cover;
}
</style>