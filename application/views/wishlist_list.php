<div class="container">
    <h1>Wishlist</h1>
    <div class="list-group">
    <?php
        foreach($wishlist->result() as $list){
            echo '<a href="'.site_url('profile/showList/'.$list->wishlist_id).'" class="list-group-item d-flex list-group-item-action justify-content-between align-items-center">';
            echo '<h5 class="mb-1">'.$list->name.'</h5>';
            echo '<small>'.$list->date.'</small>';
            echo '<span class="badge badge-primary badge-pill">'.$list->total.'</span>';
            echo '</a>';
        }
        ?>
    </div>
</div>