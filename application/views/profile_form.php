<div class="container">
    <h1>Profile</h1>
    <?php 
    foreach($user->result() as $info){
        $username = $info->username;
        $email = $info->email;
        $firstname = $info->firstname;
        $lastname = $info->lastname;
        $phone = $info->phone;
    }
    ?>
<div class="row">
    <div class="col">
        <div class="">
            <?php if(isset($update_form)){ echo form_open_multipart('profile/do_upload_info'); }?>
            <div class="row">
                <!-- User name -->
                <div class="form-group col-md-4">
                    <label for="productName">Username</label>
                    <input type="text" class="form-control" name="userName" id="userName" value="<?php echo $username;?>" disabled>
                </div>
                <!-- User email -->
                <div class="form-group col-md-8">
                    <label for="productName">Email</label>
                    <input type="email" class="form-control" name="userEmail" id="userEmail" value="<?php echo $email;?>" <?php if(isset($update_form)){ echo 'required'; } else {echo 'disabled'; } ?>>
                </div>
            </div>
            <div class="row">
                <!-- User firstname -->
                <div class="form-group col">
                    <label for="productName">Firstname</label>
                    <input type="text" class="form-control" name="userFirst" id="userFirst" value="<?php echo $firstname;?>" <?php if(isset($update_form)){ echo 'required'; } else {echo 'disabled'; } ?>>
                </div>
                <!-- User lastname -->
                <div class="form-group col">
                    <label for="productName">Lastname</label>
                    <input type="text" class="form-control" name="userLast" id="userLast" value="<?php echo $lastname;?>" <?php if(isset($update_form)){ echo 'required'; } else {echo 'disabled'; } ?>>
                </div>
                <!-- Product quantity -->
                <div class="col form-group">
                    <label for="productQty">Phone</label>
                    <input type="number" class="form-control" name="userPhone" id="userPhone" value="<?php echo $phone;?>" <?php if(isset($update_form)){ echo 'required'; } else {echo 'disabled'; } ?>>
                </div>
            </div>
            <?php 
            if(isset($update_form)){ 
                echo '<button type="submit" class="btn btn-primary" style="float: right !important;margin-bottom: 50px;">Update</button>'; 
            } else {
                echo form_open_multipart('profile/set_upload_form'); 
                echo '<button type="sumit" class="btn btn-primary" style="float: right !important;margin-bottom: 50px;">Edit profile</button>'; 
                echo form_close();
            } 
            ?>
            <?php if(isset($update_form)){ echo form_close(); } ?>
        </div>
    </div>
    <div class="col">
    <div id='map' style='width: 400px; height: 300px;'></div>
    <script>
        var center = {};
        if (navigator.geolocation.getCurrentPosition(showPosition)) {
           navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
           center = { coords : {longitude: 153.013127,latitude: -27.496932}};
           showPosition(center);
        }


        function showPosition(position) {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            userPosition = {lng:longitude,lat:latitude};
            console.log(userPosition);
            mapboxgl.accessToken = 'pk.eyJ1IjoiamFpbWUxNjkyIiwiYSI6ImNrMG01NjRtNDEzODYzbm12ZWd4Zm82NHgifQ.4bbxwtQ5W7WIRyE6oZHJ7A';
            var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: userPosition,
            zoom: 14
            });
            var marker = new mapboxgl.Marker()
            .setLngLat(userPosition)
            .addTo(map);
        }
    </script>
    </div>
</div>
</div>