<div class="container">
<?php echo form_open('product/add_product_form'); ?>
<h1 align="center" style="color: #ff626d;">Products<button type="submit" class="btn" style="color: #ff626d;"><i class="far fa-plus-square"></i></button></h1>
<?php echo form_close(); ?>  
<div class="container filter-container" style="width:100%;padding:20px;">
<div class="form-row align-items-center"  style="margin:auto;">
    <div class="col-md-4">
      <div class="input-group row">
        <label for="category" class="col-auto">Category</label>
        <select id="category" class="form-control col-auto">
          <option value="" selected>Choose...</option>
          <?php
              $navbarCategory = (isset($navCategory))? $navCategory : '-1';
              foreach($categories->result() as $category){
                $selected = ($navbarCategory == $category->category_id)? "selected" : "";
                echo '<option value="'.$category->category_id.'" '.$selected.'>'.$category->cat_name.'</option>';
              }
          ?>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="input-group row">
        <label for="type" class="col-auto">Type</label>
        <select id="type" class="form-control col-auto">
          <option  value="" selected>Choose...</option>
          <?php
              $navbarType = (isset($navType))? $navType : '-1';
              foreach($types->result() as $type){
                $selected = ($navbarType == $type->type_id)? "selected" : "";
                echo '<option value="'.$type->type_id.'" '.$selected.'>'.$type->type_name.'</option>';
              }
          ?>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="input-group row">
        <label for="sorting" class="col-auto">Sort price</label>
        <select id="sorting" class="form-control col-auto">
          <option value="DESC" selected>High to low</option>
          <option value="ASC" >Low to high</option>
        </select>
      </div>
    </div>
  </div>
</div>
  <div class="row" id="productSection">
    <?php 
        $img_url = base_url().'assets/images/';
        foreach($products->result() as $product){
            echo '<div class="col-md-3 offset-xs-1">';
            echo '<div class="card" style="width:100%;">';
            echo '<div class="card-header">'.$product->name.'</div>';
            echo '<img src="'.$img_url.$product->image.'" class="card-img-top" alt="...">';
            echo '<div class="card-body">';
            echo "<h6>".$product->cat_name." - ".$product->type_name."</h6>";
            echo '<p class="card-text">'.$product->description.'</p>';
            echo '</div>';
            echo '</div>';
            echo '<div class="card card-info offset-xs-1" style="width:100%;">';
            echo '<div class="card-header">'.$product->name.'</div>';
            echo '<div class="card-body">';
            echo "<h6>".$product->cat_name." - ".$product->type_name."</h6>";
            echo "<h4>(id ".$product->user.") ".$product->username."</h4>";
            echo "<h2>$$product->price</h2>";
            echo form_open('main/add_item_cart');
            echo '<input class="form-control" style="display:none;" type="checkbox" id="productItem" name="productItem" value="'.$product->product_id.'" checked required>';
            echo '<button type="submit" class="btn btn-success btn-block">Add to cart</button>';
            echo form_close();
            echo '<a href="'.site_url('product/publication/'.$product->product_id).'" class="btn btn-secondary btn-block" >Explore</a>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
    ?>
  </div>
</div>
<script type="text/javascript">
    $("#category").change( function() {
      event.preventDefault();
      $.post("<?= site_url('main/filter_search') ?>", { category: $("#category").val(), type: $("#type").val(), sort: $("#sorting").val(), text: "<?= (isset($search))? $search: ""; ?>" }, function (data) {
                console.log(data);
                data = $.parseJSON(data);
                return process(data);
            });
    });
    $("#type").change( function() {
      event.preventDefault();
      $.post("<?= site_url('main/filter_search') ?>", { category: $("#category").val(), type: $("#type").val(), sort: $("#sorting").val(), text: "<?= (isset($search))? $search: ""; ?>" }, function (data) {
                console.log(data);
                data = $.parseJSON(data);
                return process(data);
      });
    });
    $("#sorting").change( function() {
      event.preventDefault();
      $.post("<?= site_url('main/filter_search') ?>", { category: $("#category").val(), type: $("#type").val(), sort: $("#sorting").val(), text: "<?= (isset($search))? $search: ""; ?>" }, function (data) {
                console.log(data);
                data = $.parseJSON(data);
                return process(data);
            });
    });
    $( document ).ready( function() {
      $.post("<?= site_url('main/filter_search') ?>", { category: $("#category").val(), type: $("#type").val(), sort: $("#sorting").val(), text: "<?= (isset($search))? $search: ""; ?>" }, function (data) {
                console.log(data);
                data = $.parseJSON(data);
                return process(data);
            });
    });
    function process(data){
      var html = "";
      if(data.length > 0){
        data.forEach(function(entry) {
          html += '<div class="col-md-3 offset-xs-1">';
          html += '<div class="card" style="width:100%;">';
          html += '<div class="card-header">'+entry.name+'</div>';
          html += '<img src="<?=$img_url;?>'+entry.image+'" class="card-img-top" alt="...">';
          html += '<div class="card-body">';
          html += "<h6>"+entry.cat_name+" - "+entry.type_name+"</h6>";
          html += '<p class="card-text">'+entry.description+'</p>';
          html += '</div>';
          html += '</div>';
          html += '<div class="card card-info offset-xs-1" style="width:100%;">';
          html += '<div class="card-header">'+entry.name+'</div>';
          html += '<div class="card-body">';
          html += "<h6>"+entry.cat_name+" - "+entry.type_name+"</h6>";
          html += "<h4>(id "+entry.user+") "+entry.username+"</h4>";
          html += "<h2>$"+entry.price+"</h2>";
          html += '<form action="<?= site_url('main/add_item_cart');?>" method="post" accept-charset="utf-8">';
          html += '<input class="form-control" style="display:none;" type="checkbox" id="productItem" name="productItem" value="'+entry['product_id']+'" checked required>';
          html += '<button type="submit" class="btn btn-warning btn-block">Add to cart</button>';
          html += "<?= form_close(); ?>";
          html += '<a href="<?=site_url('product/publication/');?>'+entry['product_id']+'" class="btn btn-outline-light btn-block" style="margin-top:10px;">Explore</a>';
          html += '</div>';
          html += '</div>';
          html += '</div>';


        });
      }
      document.getElementById("productSection").innerHTML = html;
    }
</script>