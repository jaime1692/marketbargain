
<div class="register container">
<h1>Register</h1>
<p>Create a new account for a new user</p>
<?= form_open('auth/do_insert_info'); ?>
        <div class="row">
            <!-- User name -->
            <div class="form-group col-md-4">
                <label for="productName">Username</label>
                <input type="text" class="form-control" name="userName" id="userName" value="<?= set_value('userName') ?>" required>
            </div>
            <!-- User email -->
            <div class="form-group col-md-8">
                <label for="productName">Email</label>
                <input type="email" class="form-control" name="userEmail" id="userEmail" value="<?= set_value('userEmail') ?>" required>
            </div>
        </div>
        <div class="row">
            <!-- User password -->
            <div class="form-group col">
                <label for="productName">Password</label>
                <input type="password" class="form-control" name="userPassword" id="userPassword" required>
            </div>
            <!-- User password check -->
            <div class="form-group col">
                <label for="productName">Password check</label>
                <input type="password" name="userPassword2" id="userPassword2" class="form-control" required>
            </div>
        </div>
        <div class="row">
            <!-- User firstname -->
            <div class="form-group col">
                <label for="productName">Firstname</label>
                <input type="text" class="form-control" name="userFirst" id="userFirst" value="<?= set_value('userFirst') ?>" required>
            </div>
            <!-- User lastname -->
            <div class="form-group col">
                <label for="productName">Lastname</label>
                <input type="text" class="form-control" name="userLast" id="userLast" value="<?= set_value('userLast') ?>" required>
            </div>
            <!-- User phone numer -->
            <div class="col form-group">
                <label for="productQty">Phone</label>
                <input type="number" class="form-control" name="userPhone" id="userPhone" value="<?= set_value('userPhone') ?>" required>
            </div>
        </div>
        <div class="form-group" style="width:200px;margin-left:auto;" align="center">
            <?= $image['image']; ?>
            <input type="text"  class="form-control" name="captchaText" id="captchaText" value="<?= $image['word'] ?>" hidden/>
            <input type="text"  class="form-control" name="captcha" id="captcha" value="" />
            <small id="captchaHelpBlock" class="form-text text-muted">
            Enter the characters
            </small>
        </div>
        <div class="register-button">
            <button type="submit" class="btn btn-primary" style="margin-left:auto;">Create account</button>
        </div>
<?= form_close(); ?>
</div>
<style>
body{
    background-size: cover;
}
</style>