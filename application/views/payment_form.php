<div class="container">
<h1>Payment details</h1>
<?= form_open('checkout/do_payment','', $hidden = array('address' => $address)); ?>
  <div class="form-row">
    <div class="col-md-6">
      <label for="userName">Name on the Card</label>
      <input type="text" class="form-control" name="userName" id="userName" required>
    </div>
    <div class="col-md-6">
      <label for="userNumber">Card number</label>
      <input type="numer" class="form-control" name="userNumber" id="userNumber" required>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-5">
      <label for="userDay">Expiration day</label>
      <input type="number" class="form-control" name="userDay" id="userDay" required>
    </div>
    <div class="col-md-5">
      <label for="userMonth">Expiration Month</label>
      <input type="number" class="form-control" name="userMonth" id="userMonth"  required>
    </div>
    <div class="col-md-2">
      <label for="userCVC">CVC</label>
      <input type="number" class="form-control" name="userCVC" id="userCVC" required>
    </div>
  </div>
  <br>
  <input name="cart" type="text" value="<?= base64_encode(serialize($cart)); ?>" hidden>
  <button class="btn btn-primary" type="submit">Next step <i class="fas fa-arrow-right"></i></button>
<?= form_close(); ?>
</div>