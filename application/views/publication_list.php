<div class="container">
    <h1>Publications</h1>
    <div class="list-group" style="max-width: 500px;margin:auto;">
    <?php
        if(count($products->result_array()) > 0){
            foreach($products->result() as $product){
                echo '<a href="'.site_url('product/publication/'.$product->product_id).'" class="list-group-item list-group-item-action">';
                echo '<div class="d-flex w-100 justify-content-between">';
                echo '<h5 class="mb-1">'.$product->name.'</h5>';
                echo '<small>'.$product->daytime.'</small>';
                echo '</div>';
                echo '<p class="mb-1">'.$product->description.'</p>';
                echo '<p>Price $'.$product->price.'</p>';
                echo '<small>'.$product->type_name.'</small>';
                echo '</a>';
            }
        } else {
            echo "<p>You haven't publish any product yet.</p>";
        }
        ?>
    </div>
</div>