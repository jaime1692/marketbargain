<div class="container">
<h1>Trending products</h1>
    <div id="carouselProductsCard" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
        <?php 
            $img_url = base_url().'assets/images/';
            $i = 0;
            $first = true;
            foreach($favorite as $product){
                if($i == 0){
                    if($first){
                        echo '<div class="carousel-item active" data-interval="3000">';
                        $first = false;
                    } else {
                        echo '<div class="carousel-item" data-interval="3000">';
                    }
                    echo '<div class="row">';
                } 
                echo '<div class="card col">';
                echo '<img src="'.$img_url.$product->image.'" class="card-img-top" alt="">';
                echo '<div class="card-body">';
                echo '<h5 class="card-title">'.$product->name.'</h5>';
                echo '<p class="card-text" style="height:45px;overflow:hidden;">$'.$product->description.'</p>';
                echo '<h5 class="card-title">$'.$product->price.'</h5>';
                echo '<a href="'.site_url('product/publication/'.$product->product_id).'" class="btn btn-primary btn-block" >Explore</a>';
                echo '</div>';
                echo '</div>';               
                
                if($i == 3){
                    echo '</div></div>';
                    $i = 0;
                } else {
                    $i++;
                }
            }
            if($i != 0){
                echo '</div></div>';
            }
        ?>
        </div>
        <a class="carousel-control-prev" href="#carouselProductsCard" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselProductsCard" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<script>
$('.carousel').carousel()
</script>