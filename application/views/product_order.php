<?php
if(isset($item)){
    $product = $item->result()[0];
    $hidden = ['productid' => $product->product_id];
}
?>
<div class="container">
    <h1>Product item</h1>
    <div class="row">
        <div class="col">
            <!-- Product image -->
            <label for="productImage">Product image</label>
            <?php 
                $img_url = base_url().'assets/images/';
                if(isset($product->image)){
                    echo '<img src="'.$img_url.$product->image.'" class="card-img-top" alt="...">';    
                } else {
                    echo '<img src="'.$img_url.'no-photo.gif" class="card-img-top" alt="...">';
                }
            ?>
        </div>
        <div class="col">
                <h2>Your review</h2>
                <!-- Product name -->
                <div class="form-group">
                    <label for="productName">Name</label>
                    <input type="text" class="form-control" name="productName" id="productName" value="<?= $product->name ?>" disabled>
                </div>
                <?php echo form_open('product/add_rating',  array('class'=> 'form-inline','style'=>'width: fit-content;margin: auto;'), $hidden); ?>
                    <!-- Product quantity -->
                    <div class="form-group mb-2">
                        <label for="productQty">Rating</label>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <input type="number" class="form-control" name="productRating" id="productRating" value="<?= (isset($userRate))? $userRate->rate : ""; ?>" <?= (isset($userRate))? "disabled" : ""; ?>>
                    </div>
                    <!-- Product price -->
                    <button type="submit" class="btn btn-primary  mb-2" <?= (isset($userRate))? "hidden" : ""; ?>>Add rating</button>
                <?php echo form_close(); ?>
                <?php echo form_open('product/add_review','',$hidden); ?>
                <!-- Description -->
                <div class="form-group mb-3">
                <label for="productDescription">Review</label>
                <textarea class="form-control" name="productReview" id="productReview" <?= (isset($userReview))? "disabled" : ""; ?>><?php if(isset($userReview)){ echo $userReview->review;} ?></textarea>
                </div>
                <div>
                    <button type="submit" class="btn btn-primary" style="float: right !important;margin-bottom: 50px;" <?= (isset($userReview))? "hidden" : ""; ?>>Add review</button>
                </div>
                <?php echo form_close(); ?>
        </div>
    </div>
</div>