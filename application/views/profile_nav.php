<div class="row">
    <div class="col-md-3" style="background: #fdfdfd;box-shadow: 1px 0px 6px 0px #a0a0a0;padding: 0px;">
        <div class="list-group">
            <a  href="<?= site_url('profile') ?>" class="list-group-item list-group-item-action" style="padding-left:35px;">Profile</a>
            <a  href="<?= site_url('profile/wishlist') ?>" class="list-group-item list-group-item-action" style="padding-left:35px;">Wishlist</a>
            <a  href="<?= site_url('profile/publications') ?>" class="list-group-item list-group-item-action" style="padding-left:35px;">Publications</a>
            <a  href="<?= site_url('profile/orders') ?>" class="list-group-item list-group-item-action" style="padding-left:35px;">Orders</a>
            <a  href="<?= site_url('profile/inbox') ?>" class="list-group-item list-group-item-action" style="padding-left:35px;">Inbox</a>
        </div>
    </div>
    <div class="col-md-9" style="min-height:470px;">

