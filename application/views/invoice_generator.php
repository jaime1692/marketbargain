<?php

$pdf = new FPDF();
$logo = base_url()."/assets/images/watermark.png";
// Document header
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
$pdf->Image($logo,20,12,40);
$pdf->cell(80);
$pdf->cell(40,20,'Invoice for Order: #'.$order->order_id);
$pdf->Ln();
$pdf->SetFont('helvetica','I',10);
// Order details
$pdf->cell(190,7,'Order details',1,2);
$pdf->SetFont('helvetica','',10);
$y = $pdf->getY();
$status = ($payment->status)? "Paid" : "Pending";
$pdf->MultiCell(95,8,'Order id : #'.$order->order_id."\r\nOrder Status : ".$status,'LRB',1);
$x = $pdf->getX();
$pdf->setXY($x+95,$y);
$pdf->MultiCell(95,8,"Payment method : Creditcard\r\nDate : ".$order->datetime,'LRB',1);
$pdf->Ln();
$pdf->SetFont('helvetica','I',10);
// User details
$pdf->cell(190,7,'Customer details',1,2);
$pdf->SetFont('helvetica','',10);
$y = $pdf->getY();
$status = ($payment->status)? "Paid" : "Pending";
$pdf->cell(95,8,'Personal details',1,2);
$pdf->MultiCell(95,8,'Name : '.$user->firstname." ".$user->lastname."\r\nEmail : ".$user->email."\r\n\r\n\r\n",'LRB',1);
$x = $pdf->getX();
$pdf->setXY($x+95,$y);
$pdf->cell(95,8,'Shipping details',1,2);
$unit = ($address->unit != "")? " Unit ".$address->unit: "";
$address_text = " ".$address->number." ".$address->street.$unit;
$space = "                 ";
$pdf->MultiCell(95,8,"Name : ".$address->firstname." ".$address->lastname."\r\nAddress : ".$address_text."\r\n".$space.$address->city.", ".$address->country."\r\n".$space.$address->postal,'LRB',1);
$pdf->Ln();
// Order list
//print_r($cart);
    $header = array('Name','Type','Track number','Quantity','price','Total');
    // Colors, line width and bold font
    $pdf->SetFillColor(46,113,116);
    $pdf->SetTextColor(255);
    $pdf->SetDrawColor(128,0,0);
    $pdf->SetLineWidth(.3);
    $pdf->SetFont('','B');
    // Header
    $w = array(35,30,35,30,30,30);
    for($i=0;$i<count($header);$i++)
        $pdf->Cell($w[$i],7,$header[$i],1,0,'C',true);
    $pdf->Ln();
    // Color and font restoration
    $pdf->SetFillColor(224,235,255);
    $pdf->SetTextColor(0);
    $pdf->SetFont('');
    // Data
    $fill = false;
    $total = 0;
    foreach($cart as $row)
    {
        $pdf->Cell($w[0],6,$row->name,'LR',0,'L',$fill);
        $pdf->Cell($w[1],6,$row->type_name,'LR',0,'L',$fill);
        $pdf->Cell($w[2],6,$row->track_num,'LR',0,'R',$fill);
        $pdf->Cell($w[3],6,number_format($row->order_quantity),'LR',0,'R',$fill);
        $pdf->Cell($w[4],6,number_format($row->price),'LR',0,'R',$fill);
        $pdf->Cell($w[4],6,number_format($row->order_quantity*$row->price),'LR',0,'R',$fill);
        $pdf->Ln();
        $total += $row->order_quantity*$row->price;
        $fill = !$fill;
    }

    // Closing line
    $pdf->Cell(array_sum($w),0,'','T');
    $pdf->SetFillColor(95,185,189);
    $pdf->SetTextColor(255);
    $y = $pdf->getY();
    $pdf->setXY($x+130,$y);
    $pdf->Cell($w[4],6,"SubTotal",'LR',0,'R',$fill);
    $pdf->Cell($w[4],6,number_format($total),'LR',0,'R',$fill);
    $pdf->Ln();
    $y = $pdf->getY();
    $pdf->SetFillColor(46,113,116);
    $pdf->setXY($x+130,$y);
    $pdf->Cell($w[4],6,"Total",'LR',0,'R',$fill);
    $pdf->Cell($w[4],6,number_format($total),'LR',0,'R',$fill);


$pdf->Output();



