<?php $cart = $cart->result();?>
<div class="container">
<h1>Order details</h1>
<h2>Order id #<?= $order->result()[0]->order_id ?></h2>
<p><?= $order->result()[0]->datetime ?></p>
<table class="table table-borderless">
  <thead class="thead-dark">
    <tr>
      <th scope="col"></th>
      <th scope="col">Name</th>
      <th scope="col">Description</th>
      <th scope="col">Track code</th>
      <th scope="col">Type</th>
      <th scope="col">Qty</th>
      <th scope="col">Price</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <?php
        $total = 0;
        if(isset($cart)){
            for($i=0;$i < count($cart); $i++){
              $total += $cart[$i]->price * $cart[$i]->order_quantity;
                echo '<tr>';
                echo '<td><img src="'.base_url().'/assets/images/'.$cart[$i]->image.'" alt="Smiley face" height="42" width="42"></td>';
                echo '<td>'.$cart[$i]->name.'</td>';
                echo '<td>'.substr($cart[$i]->description, 0, 50).' <a href="'.site_url('product/publication/'.$cart[$i]->product_id).'">See more<a></td>';
                echo '<td>'.$cart[$i]->track_num.'</td>';
                echo '<td>'.$cart[$i]->type_name.'</td>';
                echo '<td>'.$cart[$i]->order_quantity.'</td>';
                echo '<td>'. $cart[$i]->price.'</td>';
                echo '<td><a href="'.site_url('product/product_order/'.$cart[$i]->product_id).'" class="btn btn-primary">Add review<a></td>';
                echo '</tr>';
            }
          }
          ?>
  </tbody>
</table>
<div style="width:fit-content;margin-left:auto;">
<?php echo '<h3> Total price $'.$total.'</h3>'; ?>
<div style="width:fit-content;margin-left:auto;">
<?= '<a href="'.site_url('profile/generate_invoice/'.$order->result()[0]->order_id).'" class="btn btn-info"><i class="fas fa-file-pdf"></i> Print invoice</a>'; ?>
</div>
</div>
</div>