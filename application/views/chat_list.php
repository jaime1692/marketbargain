<div class="container">
    <h1>Inbox</h1>
    <div class="row">
        <div class="col-md-4">
            <?php echo form_open('profile/send_request', 'class="form-inline user-form"'); ?>
            <div class="form-group mx-sm-3 mb-2">
                <input type="text" class="typeahead form-control" id="user" name="user" placeholder="Search a user">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Request</button>
            <?php echo form_close(); ?>
            <div class="list-group">
                <?php 
                    $active = (isset($current))?  $current : '-1';
                    if($chats->result()){
                        foreach($chats->result() as $chat){
                            if($chat->status == 'pending'){
                                if( $chat->id_one == $sender){
                                    echo '<a href="#" class="list-group-item list-group-item-action"><strong>'.$chat->user_two.'</strong> '.$chat->first_two.' '.$chat->last_two.'<button type="button" class="btn btn-outline-secondary" style="float: right;" disabled>'.$chat->status.'</button></a>';
                                } else {
                                    echo form_open('profile/accept_request', 'class="form-inline user-form"',$hidden = array("chat_id" => $chat->chat_id));
                                    echo '<li href="#" class="list-group-item list-group-item-action"><strong>'.$chat->user_one.'</strong> '.$chat->first_one.' '.$chat->last_one.'<button type="submit" class="btn btn-dark" style="float: right;">Accept</button></li >';
                                    echo form_close();
                                }
                            } else {
                                $selection = ($active == $chat->chat_id)? "active": "";
                                if( $chat->id_one == $sender){
                                    echo '<a href="'.site_url('/profile/open_chat/'.$chat->chat_id).'" class="list-group-item list-group-item-action '.$selection.'"><strong>'.$chat->user_two.'</strong> '.$chat->first_two.' '.$chat->last_two.'</a>';
                                } else {
                                    echo '<a href="'.site_url('/profile/open_chat/'.$chat->chat_id).'" class="list-group-item list-group-item-action '.$selection.'"><strong>'.$chat->user_one.'</strong> '.$chat->first_one.' '.$chat->last_one.'</a>';
                                }
                            }
                        }
                    } else {
                        echo '<a href="#" class="list-group-item list-group-item-action disabled">No chats created</a>';
                    }
                ?>   
            </div>
        </div>
        <div class="col-md-8">
            <div class="chatbox" id="chatbox">
            </div>
            <?php echo form_open('profile/send_message', 'class="form-inline chat-form"'); ?>
            <div class="form-group mx-sm-3 col-md-9">
                <input type="text" class="form-control" id="message" placeholder="Message" style="width: 100%;">
            </div>
            <button type="button" id="send_message" class="btn btn-success col-md-2">Send</button>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var refreshTimer = window.setInterval(refresh, 800);

    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.post("<?= site_url('profile/autocomplete_box') ?>", { query: query }, function (data) {
                data = $.parseJSON(data);
                return process(data);
            });
        }
    });

    $('input.typeahead').change(function(){
        $.post("<?= site_url('profile/autocomplete_box') ?>", { query: $("#user").val() }, function (data) {
                data = $.parseJSON(data);
                for (var i = 0; i < data.length; i++){
                    var obj = data[i];
                    if(obj['name'] == $("#user").val()){
                        input = '<input type="text" class="typeahead form-control" id="userid" name="userid" value="'+obj['user_id']+'" hidden>';
                        $(".user-form").append(input);
                    }
                }
        });
    });

    $('#send_message').click(function(){
        $.post("<?= site_url('profile/send_message') ?>", { chat: "<?= $active; ?>",message: $("#message").val() }, function (data) {
            console.log(data);
            $("#message").val("");
            refresh();
        });
    });

    function refresh(){
        $.post("<?= site_url('profile/refresh_message') ?>", { chat: "<?= $active; ?>" }, function (data) {
            data = $.parseJSON(data);
            input = "";
            for (var i = 0; i < data.length; i++){
                var obj = data[i];
                if(obj['sender'] != "<?=  $sender; ?>"){
                    input += '<p class="time-message">'+obj['datetime']+'</p>';
                    input += '<div class="alert alert-secondary" role="alert">'+obj['message']+'</div>';
                } else {
                    input += '<p class="time-message sender">'+obj['datetime']+'</p>';
                    input += '<div class="alert alert-info sender" role="alert">'+obj['message']+'</div>';
                }
            }
            var objDiv =  document.getElementById("chatbox");
            objDiv.innerHTML = input;
            objDiv.scrollTop = objDiv.scrollHeight;
        });
    }
</script>
<style>
.chatbox{
    width:100%;
    height:360px;
    padding-bottom:10px;
    margin-bottom:10px;
    overflow-y: scroll;
}
.time-message{
    margin-bottom:1px;
    font-size: 12px;
    color: grey;
}
.alert{
    width: fit-content !important;
    margin-right: auto;
}
.sender{
    width: fit-content !important;
    margin: unset;
    margin-left: auto;
}
</style>