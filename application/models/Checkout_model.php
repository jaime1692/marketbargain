<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Checkout_model extends CI_Model {

        public function insert_payment($data){
			$this->db->set($data);
            $query = $this->db->insert('payment_type');
            $payment = $this->get_last_payment_byID($data['user_id']);
            return $payment->result()[0]->payment_id;
        }

        public function insert_order($user, $pay_id, $address){
            $data = [
                'user'  => $user,
                'address' => $address,
                'payment'  => $pay_id
            ];

            $this->db->set($data);
            $query = $this->db->insert('orders');
            $order = $this->get_last_order_byID($data['user']);
            return $order->result()[0]->order_id;
        }

        public function get_last_payment_byID($id){
            $this->db->order_by('payment_id', 'DESC');
            $query = $this->db->get_where('payment_type', array('user_id' => $id));
			return $query;
        }

        public function get_last_order_byID($id){
            $this->db->order_by('order_id', 'DESC');
            $query = $this->db->get_where('orders', array('user' => $id));
			return $query;
        }

        // Get order payment 
        public function get_payment_byorder($userid,$order){
            $this->db->select('payment_type.*');
            $this->db->join('orders', 'payment_id = payment');
            $query = $this->db->get_where('payment_type',array('user_id' => $userid,'order_id' => $order));
            return $query;
        }

        public function getOrdersById($userid){
            $this->db->select('*, SUM(quantity) as total');
            $this->db->join('orders_detail', 'orders_detail.order_id = orders.order_id');
            $this->db->group_by("orders.order_id");
            $this->db->order_by("datetime","DESC");
            $query = $this->db->get_where('orders', array('user' => $userid));
			return $query;
        }

        public function getOrderByOrderId($id){
            $query = $this->db->get_where('orders', array('order_id' => $id));
			return $query;
        }

        public function getOrderCompleteById($id){
            $this->db->select('*, orders_detail.quantity as order_quantity');
            $this->db->join('products', 'product_id = product');
            $this->db->join('product_type', 'type_id = type');
            $query = $this->db->get_where('orders_detail', array('order_id' => $id));
			return $query;
        }

        public function insertProductOrder($products,$userid,$paymentid,$orderid){
            $data = array( 'user' => $userid ); 
            $queryWishlist = $this->db->insert('wishlist', $data);
            foreach($products as $product){
                $array = array(
                    'order_id' => $orderid,
                    'product' => $product['product'],
                    'track_num' => random_string('alnum', 12),
                    'quantity' => $product['wish_quantity']
                );
                $this->db->set($array);
                $this->db->insert('orders_detail');
            }     
        }

        // Get address
        public function get_address($userid,$order){
            $this->db->select('address.*');
            $this->db->join('orders', 'address_id = address');
            $query = $this->db->get_where('address',array('user_id' => $userid,'order_id' => $order));
            return $query;
        }

        // Insert address
        public function insert_address($data){
            $this->db->set($data);
            $query = $this->db->insert('address');
            $address = $this->get_last_address_byID($data['user_id']);
            return $address->result()[0]->address_id;
        }

        // Get last address by user
        public function get_last_address_byID($id){
            $this->db->order_by('address_id', 'DESC');
            $query = $this->db->get_where('address', array('user_id' => $id));
            return $query;
        }

    }