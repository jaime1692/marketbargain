<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class User_model extends CI_Model {
		// Log in
		public function login($username){
			// Validate
			$this->db->where('username', $username);
			$result = $this->db->get('user');

			if($result->num_rows() == 1){
				return $result;
			} else {
				return false;
			}
		}

		// Get user name
		public function get_user_info($userId){
			$query = $this->db->get_where('user', array('user_id' => $userId));
			return $query;
		}
		
		// Get user id
		public function get_userid($username){
			$this->db->select('user_id');
			$query = $this->db->get_where('user', array('username' => $username));
			return $query;
		}

		// Update user info
		public function update_user_info($userId,$email,$firstname,$lastname,$phone){
			$array = [
				'email'   => $email,
				'firstname'  => $firstname,
				'lastname' => $lastname,
				'phone' => $phone
			];
			$this->db->set($array);
			$this->db->where('user_id', $userId);
			$query = $this->db->update('user');
			return $query;
		}

		// Insert user info
		public function insert_user_info($username,$email,$password,$firstname,$lastname,$phone){
			// Hashing user password
			$password_encrypted = password_hash($password, PASSWORD_BCRYPT);
			$array = [
				'username' => $username,
				'email'   => $email,
				'password' => $password_encrypted,
				'firstname'  => $firstname,
				'lastname' => $lastname,
				'phone' => $phone
			];
			$this->db->set($array);
			$query = $this->db->insert('user');
			return $query;
		}

		// Check username exists
		public function check_username_exists($username){
			$query = $this->db->get_where('user', array('username' => $username));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

		// Typeahead search user box
		public function getUsersSimilarJson($username){
			// similar username
			$this->db->select('user_id,username as name');
			$this->db->like('username', $username);
			$this->db->from('user');
			$query1 = $this->db->get_compiled_select();
			// similar first name
			$this->db->select('user_id,firstname as name');
			$this->db->like('firstname', $username);
			$this->db->from('user');
			$query2 = $this->db->get_compiled_select();
			// similar last name
			$this->db->select('user_id,firstname as name');
			$this->db->like('lastname', $username);
			$this->db->from('user');
			$query3 = $this->db->get_compiled_select();
			// Union	
			$query = $this->db->query($query1 . ' UNION ' . $query2 . ' UNION ' . $query3);
			return $query;
		}
	}