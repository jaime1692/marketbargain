<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Wishlist_model extends CI_Model {
        public function insertProductWishList($products,$userid){
            $data = array( 'user' => $userid ); 
            $queryWishlist = $this->db->insert('wishlist', $data);
            if ($queryWishlist){
                $this->db->select_max('wishlist_id');
                $this->db->where('user', $userid);
                $query = $this->db->get('wishlist');
                foreach($products as $id => $number){
                    if($id != 0){
                        $array = array(
                            'wishlist' => $query->result_object()[0]->wishlist_id,
                            'product' => $id,
                            'wish_quantity' => $number
                        );
                        $this->db->set($array);
                        $this->db->insert('wishlist_item');
                    }
                }
                return true;
            } 
			return false;
        }

        public function getWishlistById($userid){
            $this->db->select('*, SUM(wish_quantity) as total');
            $this->db->join('wishlist_item', 'wishlist_id = wishlist');
            $this->db->group_by("wishlist_id");
            $query = $this->db->get_where('wishlist', array('user' => $userid));
			return $query;
        }

        public function getWishlistByListId($listid){ 
            $query = $this->db->get_where('wishlist', array('wishlist_id' => $listid));
			return $query;
        }

        public function getWishlistCompleteById($listid){            
            $this->db->join('wishlist_item', 'wishlist_id = wishlist');
            $this->db->join('products', 'product_id = product');
            $this->db->join('product_type', 'type_id = type');
            $query = $this->db->get_where('wishlist', array('wishlist_id' => $listid));
			return $query;
        }
    }