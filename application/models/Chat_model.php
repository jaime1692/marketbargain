<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Chat_model extends CI_Model {
        
        // Get chat by user
        public function get_chats_byUser($userid){
            $primary = "primary.user_id as id_one, primary.username as user_one,primary.firstname as first_one,primary.lastname as last_one";
            $secondary = "secondary.user_id as id_two, secondary.username as user_two,secondary.firstname as first_two,secondary.lastname as last_two";
            $this->db->select('chat.*,'.$primary.','.$secondary);
            $this->db->order_by('datetime', 'DESC');
            $this->db->join('user as primary','primary.user_id = user_one');
            $this->db->join('user as secondary','secondary.user_id = user_two');
            $this->db->where('user_one',$userid);
            $this->db->or_where('user_two',$userid);
            $query = $this->db->get('chat');
            return $query;
        }

        // Create a chat
        public function insert_chat($array){
            $query = $this->db->insert('chat', $array);
            return $query;
        }

        // Accept a chat
        public function accept_chat($chatid){
            $this->db->where('chat_id', $chatid);
            $query = $this->db->update('chat', array('status' => 'accepted'));
            return $query;
        }

        // Delete a char
        public function delete_chat($chatid){
            $this->db->where('chat_id', $chatid);
            $query = $this->db->update('chat', array('status' => 'deleted'));
            return $query;
        }

        // Send message
        public function send_message($chatid,$userid,$msg){
            $data = array('chat' => $chatid, 'sender' => $userid, 'message' => $msg);
            $query = $this->db->insert('message', $data);
            return $query;
        }
        
        // Send message
        public function get_messages($chatid){
            $query = $this->db->get_where('message', array('chat' => $chatid));
            return $query;
        }
    }