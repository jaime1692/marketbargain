<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Product_model extends CI_Model {
        
        // Get product types
        public function getTypes(){
            $query = $this->db->get('product_type');
            return $query;
        }

        // Get product categories
        public function getCategories(){
            $query = $this->db->get('categories');
            return $query;
        }

		// Get products
		public function getProducts(){
            $this->db->join('user', 'user_id = user');
            $this->db->join('product_type', 'type_id = type');
            $this->db->join('categories', 'category_id = category');
            $this->db->order_by('price','DESC');
			$query = $this->db->get('products');
			return $query;
        }

        // Get favorite products
        public function getFavorite(){
            $this->db->limit(8);
            $this->db->select('products.*, SUM(wish_quantity) as fav');
            $this->db->join('user','user_id = user');
            $this->db->join('wishlist_item', 'product_id = product');
            $this->db->group_by("product_id");
            $this->db->order_by('fav','DESC');
			$query = $this->db->get('products');
			return $query;
        }

        // Get products in offer
        public function getOffers(){
            $this->db->limit(5);
            $this->db->select('products.*, promotion');
            $this->db->join('offers', 'offers.product_id = products.product_id');
            $this->db->order_by('promotion','DESC');
			$query = $this->db->get('products');
			return $query;
        }

        // Search similar name
        public function getProductSimilarJson($text){
            // similar product name
            $this->db->select('products.name');
            $this->db->like('name', $text);
            $this->db->from('products');
            $query1 = $this->db->get_compiled_select();
            // similar category name
            $this->db->select('cat_name');
            $this->db->join('categories', 'category_id = category');
            $this->db->like('cat_name', $text);
            $this->db->from('products');
            $query2 = $this->db->get_compiled_select();
            // similar type name
            $this->db->select('type_name');
            $this->db->join('product_type', 'type_id = type');
            $this->db->like('type_name', $text);
            $this->db->from('products');
            $query3 = $this->db->get_compiled_select();
            // Union
            $query = $this->db->query($query1 . ' UNION ' . $query2 . ' UNION ' . $query3);
            return $query;
        }

        // Fuzzy search similar products
        public function getFuzzyProduct($fuzzy,$sort){
            $this->db->distinct('product_id');
            $this->db->join('user', 'user_id = user');
            $this->db->join('product_type', 'type_id = type');
            $this->db->join('categories', 'category_id = category');
            foreach($fuzzy as $key => $value){
                if($key == 0){
                    $this->db->like('cat_name', $value,'both', false);
                    $this->db->or_like('type_name', $value,'both',false);
                    $this->db->or_like('name', $value, 'both',false);
                    $this->db->or_like('description', $value,'both', false);
                } else {
                    $this->db->or_like('cat_name', $value,'both', false);
                    $this->db->or_like('type_name', $value,'both',false);
                    $this->db->or_like('name', $value, 'both',false);
                    $this->db->or_like('description', $value,'both', false);
                }
            }
            if ($sort != null){
                $this->db->order_by('price',$sort);
            } else {
                $this->db->order_by('price','DESC');
            }
            $query = $this->db->get('products');
            return $query;
        }

        // Search similar products
        public function getProductSimilar($text,$sort){
            $this->db->join('user', 'user_id = user');
            $this->db->join('product_type', 'type_id = type');
            $this->db->join('categories', 'category_id = category');
            $this->db->like('cat_name', $text);
            $this->db->or_like('type_name', $text);
            $this->db->or_like('name', $text);
            $this->db->or_like('description', $text);
            if ($sort != null){
                $this->db->order_by('price',$sort);
            } else {
                $this->db->order_by('price','DESC');
            }
			$query = $this->db->get('products');
			return $query;
        }
        
        // Get products by user
		public function getProductByUser($userid){
            $this->db->join('categories', 'category_id = category');
            $this->db->join('product_type', 'type_id = type');
			$query = $this->db->get_where('products', array('user' => $userid));
			return $query;
        }

        // Get products by Category
		public function getProductByCategory($cat){
            $this->db->select('*,AVG(rate) as rating');
            $this->db->join('rating', 'product_id = rating.product');
            $this->db->join('categories', 'category_id = category');
            $this->db->join('product_type', 'type_id = type');
			$query = $this->db->get_where('products', array('category_id' => $cat));
			return $query;
        }
        
        // Get products by type
		public function getProductByType($type){
            $this->db->select('*,AVG(rate) as rating');
            $this->db->join('rating', 'product_id = rating.product');
            $this->db->join('categories', 'category_id = category');
            $this->db->join('product_type', 'type_id = type');
			$query = $this->db->get_where('products', array('type_id' => $type));
			return $query;
        }

        // Get products by ID
		public function getProductById($productid){
            $this->db->select('user_id,rating.*,categories.*,product_type.*,products.*,AVG(rate) as rating');
            $this->db->join('rating', 'product_id = rating.product');
            $this->db->join('categories', 'category_id = category');
            $this->db->join('product_type', 'type_id = type');
            $this->db->join('user', 'user_id = products.user');
			$query = $this->db->get_where('products', array('product_id' => $productid));
			return $query;
        }
        
        // Get product's reviews by ID
		public function getProductReviewById($productid){
            $this->select('username, review.*');
            $this->db->join('user', 'user_id = review.user');
			$query = $this->db->get_where('review', array('product' => $productid));
			return $query;
        }
        
        // Get user product review by ID
		public function getUserReviewProduct($productid,$userid){
			$query = $this->db->get_where('review', array('product' => $productid,'user' => $userid));
			return $query;
        }
        
        // Get user product rating by ID
		public function getUserRatingProduct($productid,$userid){
			$query = $this->db->get_where('rating', array('product' => $productid,'user' => $userid));
			return $query;
        }
        
        // Update a product
		public function updateProduct($productid,$userid,$name,$description,$price,$quantity,$daytime){
            $data = array(  
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'quantity' => $quantity,
                'daytime' => $daytime
             );
            $this->db->where('user', $userid); 
            $this->db->where('product_id', $productid);
            $query = $this->db->update('products', $data);
			return $query;
        }     

		// Create a product
		public function addProduct($userid,$name,$description,$price,$type,$category,$quantity,$image,$daytime){
            $data = array( 
                'user' => $userid, 
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'type' => $type,
                'category' => $category,
                'quantity' => $quantity,
                'image' => $image,
                'daytime' => $daytime
             ); 
            $query = $this->db->insert('products', $data);
			return $query;
        }
        
        // Create product's reviews
		public function addProductReview($userid,$productid,$review){
            $data = array( 
                'user' => $userid, 
                'product' => $productid,
                'review' => $review
            );
			$query = $this->db->insert('review', $data);
			return $query;
        }
        
        // Create product's rate
		public function addProductRate($userid,$productid,$rate){
            $data = array( 
                'user' => $userid, 
                'product' => $productid,
                'rate' => $rate
            );
			$query = $this->db->insert('rating', $data);
			return $query;
		}
	}