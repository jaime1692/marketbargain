<?php
class Auth extends CI_Controller {
	public function login(){
		// Render header
		$this->load->view('templates/header');
		// Render navbar
		$site_data = get_site_details();
		$this->load->view('templates/navbar',$site_data);
		// Checking user auth
		if(!$this->session->userdata('logged_in')){
			$username = $this->input->post("username");
			$password = $this->input->post("password");
			$remember = $this->input->post("rememberme");
			$this->check_user($username,$password,$remember);
		} else {
			return redirect('main');
		}
		$this->load->view('templates/footer');
	}

	public function register(){
		// Load user model
		$this->load->model('user_model');
		// Render header
		$this->load->view('templates/header');
		// Render navbar
		$site_data = get_site_details();
		$this->load->view('templates/navbar',$site_data);
		// Register form
		$data['image'] = $this->generateCaptcha();
		$this->load->view('register_form',$data);
		// Render Footer
		$this->load->view('templates/footer');
	}

	public function do_insert_info(){
		// user form input
		$name = $this->input->post("userName");
        $email = $this->input->post("userEmail");
		$password = $this->input->post("userPassword");
		$password2 = $this->input->post("userPassword2");
        $first = $this->input->post("userFirst");
        $last = $this->input->post("userLast");
		$phone = $this->input->post("userPhone");
		$capUser = $this->input->post("captcha");
		$cap = $this->input->post("captchaText");
		$error = $this->checkPassword($password);
		$error = $this->checkTwoPass($password,$password2);
		$error = $this->checkCaptcha($cap,$capUser);
		if(isset($error)){
			print_r($error);
			$this->register();
		} else {
			// Sending info to the user model
			$this->load->model('user_model');
			$status = $this->user_model->insert_user_info($name,$email,$password,$first,$last,$phone);
			if($status){
				return  $this->check_user($name,$password,false);
			} else {
				print_r('<div class="alert alert-warning" role="alert">Error in the insertion</div>');
				$this->register();
			}
		}
	}

	public function logout(){
		// Unset user data
		if(!$this->session->userdata('remember')){
			$this->session->unset_userdata('logged_in');
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('userid');
			$this->session->unset_userdata('remember');
		}
		return redirect('auth/login');
	}

	private function check_user($username,$password,$remember){
		$this->load->model('user_model');
		$user_checked = $this->user_model->login($username);
		if($user_checked){
			$hash = $user_checked->result_array()[0]['password'];
			if(password_verify($password, $hash)){
				$userid = $this->user_model->get_userid($username);
				$userid = $userid->result_array()[0]['user_id'];
				// Create user session
				$user_data = array(
					'username' => $username,
					'userid' => $userid,
					'remember' => $remember,
					'logged_in' => true
				);
				// Set user data
				$this->session->set_userdata($user_data);
				return redirect('main');
			} else {
				print_r('<div class="alert alert-warning" role="alert"><strong>Error!</strong> username or password is incorrect</div>');
				$this->load->view('login');
			}
		} else {
			$this->load->view('login');
		}
	}

	public function check_email(){
		$this->email->from('jaimeg1606@gmail.com', 'Jaime Garzon');
		$this->email->to('yuhui0512@gmail.com');

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');

		$this->email->send();
	}

	public function generateCaptcha(){
		// Setting captcha parameters
		$vals = array(
			'word'          => random_string('alnum', 8),
			'img_path'      => './assets/captcha/',
			'img_url'       => base_url('assets/captcha'),
			'font_path'     => './path/to/fonts/texb.ttf',
			'img_width'     => '150',
			'img_height'    => 50,
			'expiration'    => 7200,
			'word_length'   => 8,
			'font_size'     => 22,
			'img_id'        => 'ImageCatcha',
			'colors'        => array(
				'background' => array(255, 255, 255),
				'border' => array(255, 255, 255),
				'text' => array(0, 0, 0),
				'grid' => array(255, 40, 40)
			)
		);
		// Creating captcha
		$cap = create_captcha($vals);
		$this->submit_captcha($cap);
		return $cap;
	}

	private function checkPassword($pwd){
		if (strlen($pwd) < 8) {
			return '<div class="alert alert-warning" role="alert">Password too short!</div>';
		}
		if (!preg_match("#[0-9]+#", $pwd)) {
			return '<div class="alert alert-warning" role="alert">Password must include at least one number!</div';
		}
		if (!preg_match("#[a-zA-Z]+#", $pwd)) {
			return '<div class="alert alert-warning" role="alert">Password must include at least one letter!</div>';
		}     
	}

	private function checkTwoPass($pwd1,$pwd2){
		if ($pwd1 != $pwd2) {
			return '<div class="alert alert-warning" role="alert">The two passwords didn\'t match</div>';
		}
	}

	private function checkCaptcha($cap,$text){
		if ($text != $cap) {
			return '<div class="alert alert-warning" role="alert">The captcha text entered is incorred</div>';
		}
	}

	private function submit_captcha($captcha){
		$data = array(
			'captcha_time'  => $captcha['time'],
			'ip_address'    => $this->input->ip_address(),
			'word'          => $captcha['word']
		);
		// Load captcha model
		$this->load->model('captcha_model');
		$this->captcha_model->insert_captcha($data);
	}
}