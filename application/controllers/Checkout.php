<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {
    
    public function index(){
        $data = $this->set_structure();
        $data['cart'] = unserialize(base64_decode($this->input->post("cart")));
        $this->load->view('shipping_form',$data);
        // Load footer
        $this->load->view('templates/footer');
    }

    public function payment($address){
        $data = $this->set_structure();
        $data['cart'] = unserialize(base64_decode($this->input->post("cart")));
        $data['address'] = $address;
        $this->load->view('payment_form',$data);
        // Load footer
        $this->load->view('templates/footer');
    }

    public function success_checkout(){
        $data = $this->set_structure();
        $data['cart'] = unserialize(base64_decode($this->input->post("cart")));
        $this->load->view('checkout_sucess',$data);
        // Load footer
        $this->load->view('templates/footer');
    }

    public function do_insert_address(){
        $array = [
            'user_id'  => $this->session->userdata("userid"),
            'firstname'  =>  $this->input->post("userFirst"),
            'lastname' => $this->input->post("userLast"),
            'unit' => $this->input->post("userUnit"),
            'number'   => $this->input->post("userNumber"),
            'street'   => $this->input->post("userStreet"),
            'city'  => $this->input->post("userCity"),
            'country' => $this->input->post("userCountry"),
            'postal' => $this->input->post("userPostal"),
            'phone' => $this->input->post("userPhone")
        ];
        $this->load->model('checkout_model');
        $address_id = $this->checkout_model->insert_address($array);
        return $this->payment($address_id);
    }

    public function do_payment(){
        $userid = $this->session->userdata("userid");
        $address = $this->input->post("address");
        $data['cart'] = unserialize(base64_decode($this->input->post("cart")));
        $array = [
            'user_id'  => $userid,
            'name'  =>  $this->input->post("userName"),
            'number' => $this->input->post("userNumber"),
            'exp_month' => $this->input->post("userDay"),
            'exp_year'   => $this->input->post("userMonth"),
            'cvc'   => $this->input->post("userCVC"),
            'amount'  => 100,
            'currency' => 'AUD',
            'status' => true
        ];
        $this->load->model('checkout_model');
        $payment_id = $this->checkout_model->insert_payment($array);
        $order_id = $this->checkout_model->insert_order($this->session->userdata("userid"),$payment_id,$address);
        $this->checkout_model->insertProductOrder($data['cart'],$userid,$payment_id,$order_id);
        return $this->success_checkout();
    }

    private function set_structure(){
        is_logged_in();
        // Load header
		$this->load->view('templates/header');
        // Load navbar
        $site_data = get_site_details();
        $this->load->view('templates/navbar',$site_data);
        // Render product form
        $this->load->model('user_model');
        $data['user'] = $this->user_model->get_user_info($this->session->userdata('userid'));
        return $data;
    }

}