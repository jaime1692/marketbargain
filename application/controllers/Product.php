<?php

defined('BASEPATH');

class Product extends CI_Controller {

    public function publication($id = null){
        is_logged_in();
        // Load header
		$this->load->view('templates/header');
        // Load navbar
        $site_data = get_site_details();
        $this->load->view('templates/navbar',$site_data);
        // Render product form
        $this->load->model('product_model');
        $data['item'] = $this->product_model->getProductById($id);
        $merged_data = array_merge($data,$site_data);
        $this->load->view('product_public',$merged_data);
        $favorite['favorite'] = $this->product_model->getFavorite()->result();
        $this->load->view('carousel_cards',$favorite);
        // Load footer
        $this->load->view('templates/footer');
    }

    public function product_order($id = null){
        is_logged_in();
        $user = $this->session->userdata('userid');
        // Load header
		$this->load->view('templates/header');
        // Load navbar
        $site_data = get_site_details();
        $this->load->view('templates/navbar',$site_data);
        // Render product form
        $this->load->model('product_model');
        $data['item'] = $this->product_model->getProductById($id);
        $data['userReview'] = $this->product_model->getUserReviewProduct($id,$user)->first_row();
        $data['userRate'] = $this->product_model->getUserRatingProduct($id,$user)->first_row();
        $merged_data = array_merge($data,$site_data);
        $this->load->view('product_order',$merged_data);
        // Load footer
        $this->load->view('templates/footer');
    }

    public function add_rating(){
        $user = $this->session->userdata('userid');
        $product = $this->input->post("productid");
        $rate = $this->input->post("productRating");
        // Load products model
        $this->load->model('product_model');
        $this->product_model->addProductRate($user,$product,$rate);
        return $this->product_order($product);
    }

    public function add_review(){
        $user = $this->session->userdata('userid');
        $product = $this->input->post("productid");
        $review = $this->input->post("productReview");
        // Load products model
        $this->load->model('product_model');
        $this->product_model->addProductReview($user,$product,$review);
        return $this->product_order($product);
    }

    public function add_Product_form(){
        $data = array();
        $this->setForm($data);
    }

    public function do_upload(){
        $config['upload_path'] = './assets/images';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 1000000;
        $config['max_width'] = 10000;
        $config['max_height'] = 7000;
        $this->load->Library('upload',$config);
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('image')){
            $data = array('error' =>  $this->upload->display_errors());
            $this->setForm($data);
        } else {
            $data["upload_data"] = $this->upload->data();
            $this->resizing('./assets/images/'.$data["upload_data"]['file_name']);
            $this->watermark('./assets/images/'.$data["upload_data"]['file_name']);
            $this->setForm($data);
        }
    }

    public function add_product(){
        // Load user model
        $this->load->model('user_model');
        // Get user id
        $userid = $this->session->userdata('userid');
        $name = $this->input->post("productName");
        $description = $this->input->post("productDescription");
        $type = $this->input->post("productType");
        $category = $this->input->post("productCategory");
        $quantity = $this->input->post("productQty");
        $price = $this->input->post("productPrice");
        $image = $this->input->post("productImage");
        $daytime = date('Y-m-d H:i:s');
        // Load products model
        $this->load->model('product_model');
        // Add new product
        $product_added = $this->product_model->addProduct($userid,$name,$description,$price,$type,$category,$quantity,$image,$daytime);
        if($product_added){
            return redirect('main');
        } else {
            $this->setForm($data);
        }
    }

    public function update_product(){
        // Load user model
        $this->load->model('user_model');
        // Get user id
        $userid = $this->session->userdata('userid');
        $product = $this->input->post('product');
        $name = $this->input->post("productName");
        $description = $this->input->post("productDescription");
        $price = $this->input->post("productPrice");
        $quantity = $this->input->post("productQty");
        $daytime = date('Y-m-d H:i:s');
        // Load products model
        $this->load->model('product_model');
        // update new product
        $product_update = $this->product_model->updateProduct($product,$userid,$name,$description,$price,$quantity,$daytime);
        if($product_update){
            return redirect('main');
        } else {
            $this->setForm($data);
        }
    }

    private function watermark($image){
        $config['wm_type'] = 'overlay';
        $config['source_image'] = $image;
        $config['wm_overlay_path'] = './assets/images/watermark.png';
        $config['wm_opacity'] = 60;
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'center';
        $config['wm_vrt_offset'] = '10';
        
        $this->image_lib->initialize($config);
        $status = $this->image_lib->watermark();
        if (!$status){
           return $data['error'] = $this->image_lib->display_errors();
        }
    }

    private function resizing($image){
        $config['source_image'] = $image;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 1024;

        $this->image_lib->initialize($config);
        $status = $this->image_lib->resize();
        $this->image_lib->clear();
        if (!$status){
           return $data['error'] = $this->image_lib->display_errors();
        }
    }
    
    private function setForm($data){
        is_logged_in();
        // Load header
		$this->load->view('templates/header');
        // Load navbar
        $site_data = get_site_details();
        $this->load->view('templates/navbar',$site_data);
        // Render product form
        $merged_data = array_merge($data,$site_data);
        $this->load->view('product_form',$merged_data);
        // Load footer
        $this->load->view('templates/footer');
    }
}