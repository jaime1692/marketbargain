<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function index()
	{
		is_logged_in();
        // Load header
		$this->load->view('templates/header');
		// Load navbar
		$site_data = get_site_details();
        $this->load->view('templates/navbar',$site_data);
		$data["cart"] = pull_items_details();
		$this->load->view('shopping_cart', $data);
		$this->load->view('templates/footer');
	}

	public function minus_one(){
		$id = $this->input->post("id");
		$items = get_cookie('shopping_Cart');
		$item = "idprod".$id;
		$items = preg_replace( "'".$item."'",'',$items, 1);
		$this->input->set_cookie('shopping_Cart',$items,'25200'); 
		return redirect('cart');
	}

	public function plus_one(){
		$id = $this->input->post("id");
		$items = get_cookie('shopping_Cart');
		$item = "idprod".$id;
		$items .= $item;
		$this->input->set_cookie('shopping_Cart',$items,'25200'); 
		return redirect('cart');
	}

	public function save_wishlist(){
		$id = $this->session->userdata('userid');
		// Load product model
        $this->load->model('wishlist_model');
		//get shopping cart cookie
		$items = get_cookie('shopping_Cart');
		$items_array = explode("idprod",$items);
		$items_repetions = array_count_values($items_array);
		//Set data to the database
		$this->wishlist_model->insertProductWishList($items_repetions,$id);
		return redirect('main');
	}
}