<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
    public function index(){
        $data = $this->set_structure();
        $this->load->view('profile_nav');
        $this->load->view('profile_form',$data);
        // Load footer
        $this->load->view('templates/footer');
    }

    public function wishlist(){
        $data = $this->set_structure();
        $this->load->view('profile_nav');
        $this->load->model('wishlist_model');
        $userid = $this->session->userdata('userid');
        $data["wishlist"] = $this->wishlist_model->getWishlistById($userid);
        $this->load->view('wishlist_list',$data);
        $this->load->view('templates/footer');
    }

    public function publications(){
        $data = $this->set_structure();
        $this->load->view('profile_nav');
        $this->load->model('product_model');
        $userid = $this->session->userdata('userid');
        $data["products"] = $this->product_model->getProductByUser($userid);
        $this->load->view('publication_list',$data);
        $this->load->view('templates/footer'); 
    }

    public function orders(){
        $data = $this->set_structure();
        $this->load->view('profile_nav');
        $this->load->model('checkout_model');
        $userid = $this->session->userdata('userid');
        $data["orders"] = $this->checkout_model->getOrdersById($userid);
        $this->load->view('orders_list',$data);
        $this->load->view('templates/footer');
    }

    public function inbox(){
        $data = $this->set_structure();
        $this->load->view('profile_nav');
        $this->load->model('chat_model');
        $userid = $this->session->userdata('userid');
        $data["chats"] = $this->chat_model->get_chats_byUser($userid);
        $data["sender"] = $userid;
        $this->load->view('chat_list',$data);
        $this->load->view('templates/footer');
    }

    public function open_chat($id = null){
        $data = $this->set_structure();
        $this->load->view('profile_nav');
        $this->load->model('chat_model');
        $userid = $this->session->userdata('userid');
        $data["chats"] = $this->chat_model->get_chats_byUser($userid);
        $data["sender"] = $userid;
        $data["current"] = $id;
        $this->load->view('chat_list',$data);
        $this->load->view('templates/footer');
    }

    public function send_message(){
        $userSession = $this->session->userdata('userid');
        $chatid = $this->input->post('chat');
        $msg = $this->input->post('message');
        $this->load->model('chat_model');
        $status = $this->chat_model->send_message($chatid,$userSession,$msg);
        echo $status;
    }

    public function refresh_message(){
        $chat = $this->input->post('chat');
		$this->load->model('chat_model');
		$data = $this->chat_model->get_messages($chat);
        echo json_encode($data->result());
    }

    public function showList($id = null){
        $data = $this->set_structure();
        $this->load->view('profile_nav');
        $this->load->model('wishlist_model');
        $data["wishlist"] = $this->wishlist_model->getWishlistByListId($id);
        $data["cart"] = $this->wishlist_model->getWishlistCompleteById($id);
		$this->load->view('saved_wishlist', $data);
    }

    public function showOrder($id = null){
        $data = $this->set_structure();
        $this->load->view('profile_nav');
        $this->load->model('checkout_model');
        $data["order"] = $this->checkout_model->getOrderByOrderId($id);
        $data["cart"] = $this->checkout_model->getOrderCompleteById($id);
		$this->load->view('order_complete', $data);
    }

    public function generate_invoice($id = null){
        $userid = $this->session->userdata('userid');
        $this->load->model('user_model');
        $this->load->model('checkout_model');
        $data["user"] = $this->user_model->get_user_info($userid)->row();
        $data["address"] = $this->checkout_model->get_address($userid,$id)->row();
        $data["payment"] = $this->checkout_model->get_payment_byorder($userid,$id)->row();
        $data["order"] = $this->checkout_model->getOrderByOrderId($id)->row();
        $data["cart"] = $this->checkout_model->getOrderCompleteById($id)->result();
        $this->load->view('invoice_generator',$data);
    }

    public function set_upload_form(){
        $data = $this->set_structure();
        $data["update_form"] = true;
        $this->load->view('profile_form',$data);
        // Load footer
        $this->load->view('templates/footer');
    }

    public function do_upload_info(){
        $userid = $this->session->userdata('userid');
        $email = $this->input->post("userEmail");
        $first = $this->input->post("userFirst");
        $last = $this->input->post("userLast");
        $phone = $this->input->post("userPhone");

        $this->load->model('user_model');

        $this->user_model->update_user_info($userid,$email,$first,$last,$phone);

        return redirect('profile');
    }

    public function send_request(){
        $userSession = $this->session->userdata('userid');
        $userid = $this->input->post('userid');
        $data = array('user_one' => $userSession, 'user_two' => $userid);
        $this->load->model('chat_model');
        $this->chat_model->insert_chat($data);
        return $this->inbox();
    }

    public function accept_request(){
        $chat = $this->input->post('chat_id');
        $this->load->model('chat_model');
        $this->chat_model->accept_chat($chat);
        return $this->inbox();
    }

    public function autocomplete_box(){
        $query = $this->input->get('query');
		$this->load->model('user_model');
		$data = $this->user_model->getUsersSimilarJson($query);
        echo json_encode($data->result());
    }

    private function set_structure(){
        is_logged_in();
        // Load header
		$this->load->view('templates/header');
        // Load navbar
        $site_data = get_site_details();
        $this->load->view('templates/navbar',$site_data);
        // Render product form
        $this->load->model('user_model');
        $data['user'] = $this->user_model->get_user_info($this->session->userdata('userid'));
        return $data;
    }

}