<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index(){
		$data = [];
		$this->main_structure($data);
	}

	public function category($id = null){
		$this->load->model('product_model');
		$data["products"] = $this->product_model->getProductByCategory($id);
		$data["navCategory"] = $id;
		$this->main_structure($data);
	}

	public function type($id = null){
		$this->load->model('product_model');
		$data["products"] = $this->product_model->getProductByType($id);
		$data["navType"] = $id;
		$this->main_structure($data);
	}

	public function add_item_cart(){
		$itemid = $this->input->post("productItem");
		$items = get_cookie('shopping_Cart');
		if(isset($items)){
			$items = explode("idprod",$items);
			$items = array_merge($items,array($itemid));
			$items = implode("idprod",$items);
		} else {
			$items = "idprod".$itemid;

		}
		$this->input->set_cookie('shopping_Cart',$items,'25200'); 
		return redirect('main');
	}

	public function search_item(){
		$text = $this->input->post("text");
		$this->load->model('product_model');
		$data["search"] = $text;
		$data["products"] = $this->product_model->getProductSimilar($text,null);
		$data["products"] = ($data["products"]->num_rows() == 0)? $this->fuzzy_search($text,null) : $data["products"];
		$this->main_structure($data);
	}

	public function fuzzy_search($text){
		$this->load->model('product_model');
		$fuzzy = [];
		for($i=0;$i < strlen($text); $i++){
			if($i > 0){
				//Missing letters
				$fuzzy[] = substr_replace($text,'_',$i,$i-strlen($text)); 
			}
			if($i != strlen($text)-1){
				//Spelling errors
				$fuzzy[] = substr_replace($text,'_',$i,$i-strlen($text)+1); 
				//Extra letters
				$fuzzy[] = substr_replace($text,'',$i,$i-strlen($text)+1); 
			} else {
				//Spelling errors
				$fuzzy[] = substr_replace($text,'_',-1);
				//Extra letters
				$fuzzy[] = substr_replace($text,'',-1);
			}
		}
		$data = $this->product_model->getFuzzyProduct($fuzzy,null);
		return $data;
	}

	public function filter_search(){
		$text = $this->input->post("text");
		$cat = $this->input->post("category");
		$type = $this->input->post("type");
		$sort = $this->input->post("sort");
		$this->load->model('product_model');
		$temp = $this->product_model->getProductSimilar($text,$sort);
		$temp = ($temp->num_rows() == 0)? $this->fuzzy_search($text,$sort) : $temp;
		//print_r("cat=".$cat.";type=".$type.";sort=".$sort);
		foreach($temp->result() as $key => $value){
			if($cat != "" && $type != "") {
				if($value->category == $cat && $value->type == $type){
					$data[] = $temp->result()[$key];
				}
			} else if($cat != "") {
				if($value->category == $cat){
					$data[] = $temp->result()[$key];
				}
			} else if($type != ""){
				if($value->type == $type){
					$data[] = $temp->result()[$key];
				}
			} else {
				$data[] = $temp->result()[$key];
			}
		}
		echo (isset($data))? json_encode($data) : json_encode(array());
	}
	
	public function autocomplete_box(){
        $query = $this->input->get('query');
		$this->load->model('product_model');
		$data = $this->product_model->getProductSimilarJson($query,null);
        echo json_encode($data->result());
    }

	private function main_structure($data){
		// Load header
		$this->load->view('templates/header');
		// Load navbar
		$site_data = get_site_details();
		$data[] = $site_data;
		$this->load->view('templates/navbar',$site_data);
		// Check authetication
		if(!$this->session->userdata('logged_in')){
			// Redirect to login
			return redirect('auth');
		} else {
			if(!isset($data["products"])){
				// Load products model
				$this->load->model('product_model');
				$data["products"] = $this->product_model->getProducts();
			}
			$offers['offers'] = $this->product_model->getOffers()->result();
			$this->load->view('carousel_offers',$offers);
			$favorite['favorite'] = $this->product_model->getFavorite()->result();
			$this->load->view('carousel_cards',$favorite);
			$this->load->view('productsList',$data);
		}
		$this->load->view('templates/footer');
		$this->load->view('scroll_pos');
	}
}